## Fiche d'exigence

<!--
Le titre de l'issue est le libellé de l'exigence
-->

- Origine de l'exigence: [document, paragraphe, etc.]

## Détails
<!-- 
Reformuler l'exigence
-->

## Réponse
<!-- 
Expliquer de quelle manière nous répondons à l'exigence
-->

## Prise en compte et limite(s) de périmètre
<!-- 
- Est-ce que notre réponse prend en compte partiellement ou complètement l'exigence?
- Lister les limites de périmètre
-->

- Prise en compte à préciser à la fin de la réponse: ~"SCOPE::Full" ~"SCOPE::Partial" ~"SCOPE::None"

#### Limites de périmètres lorsque la prise en compte n'est pas totale ~"SCOPE::Full"

> à détailler

<!-- 
- Déterminez la nature de l'exigence
- Précisez la milestone si vous voulez gérer un notion de temps (le moment où cela doit être "réglé")
-->

/milestone %01-Qualification %02-Analysis %"03-Technical proposal" %"04-Financial proposal" %05-Submit
/label ~"PROGRESS::0%" ~ANALYSIS ~"REQ::Technical" ~"REQ::Functional" ~"REQ::Financial" ~"REQ::Legal" ~"REQ::Other"


